# _*_ encoding: utf-8 _*_
from django.db import models

# Create your models here.

class Convidado(models.Model):
	"""docstring for Convidado"""
	tipo = models.CharField(max_length=100)
	nome = models.CharField(max_length=100)
	descricao =models.TextField()
	links = models.CharField(max_length=200)
	ativo = models.BooleanField()
	imagem = models.ImageField(upload_to = 'uploads/img_para_visitantes/')

	def __unicode__(self):
		return self.nome


class Presente(models.Model):
	"""docstring for Presente 
	- NOME DO PRESENTE  - DESCRIÇÃO  - LINKS LISTA DE LINKS MAIS BARATOS  - BOOLEANO JA_GANHAMOS PARA APRESENTAR A IMAGEM DO JA GANHAMOS COMO LABEL NO CANTO DA IMAGEM
	- ATIVO já sabe né..  """

	tipo = models.CharField(max_length=100)
	nome = models.CharField(max_length=100)
	descricao =models.TextField()
	links = models.CharField(max_length=200)
	ja_ganhamos = models.BooleanField()
	ativo = models.BooleanField()
	imagem = models.ImageField(upload_to = 'uploads/img_presentes/')

	def __unicode__(self):
		return self.nome

class Fotos(models.Model):
	nome   = models.CharField(max_length=100)
	ativo  = ativo = models.BooleanField()
	imagem = models.ImageField(upload_to='uploads/img_galeria_fotos/')

	def __unicode__(self):
		return self.nome