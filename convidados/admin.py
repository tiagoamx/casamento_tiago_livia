from convidados.models import Presente, Convidado, Fotos
from django.contrib import admin

admin.site.register(Presente)
admin.site.register(Convidado)
admin.site.register(Fotos)