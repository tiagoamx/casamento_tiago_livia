# _*_ encoding: utf-8 _*_

from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response


#from models import model tal !  
from models import Presente, Convidado, Fotos

# Create your views here.

def index(request):
	lista_galeria_fotos = Fotos.objects.all().filter(ativo=True)
	lista_presentes = Presente.objects.all().filter(ativo=True).order_by('tipo')
	lista_para_convidados = Convidado.objects.all().filter(ativo=True).order_by('tipo')
	return render_to_response("index.html",
		{'lista_presentes':lista_presentes,'lista_para_convidados':lista_para_convidados,'lista_galeria_fotos':lista_galeria_fotos},
		context_instance=RequestContext(request))

def inicial(request):
	return HttpResponse(u'Oié')